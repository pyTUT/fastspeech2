* config/LJSpeech/preprocess.yaml
* prepare_align.py
> result  13100it [07:19, 29.82it/s]
* conda config --add channels conda-forge 
* conda install montreal-forced-aligner
* use MFA
  ```
    mfa model download acoustic english_us_arpa
    mfa model download dictionary english_us_arpa
    mfa validate raw_data/LJSpeech/ english_us_arpa english_us_arpa
    mfa align raw_data/LJSpeech/ lexicon/librispeech-lexicon.txt english_us_arpa preprocessed_data/LJSpeech/TextGrid/ --clean
  ```
  
* or direct download the data at [here](https://drive.google.com/drive/folders/1DBRkALpPd6FL9gjHMmMEdHODmkgNIIK4?usp=sharing)
> unzip the files in ``preprocessed_data/LJSpeech/TextGrid/``.

* pip install -r requirements.txt
* preprocess.py 
  

* train.py
* change utils/model.py to utils_model.py 
* change utils/tools.py to utils_tools.py
* unzip hifigan/generator_LJSpeech.pth.tar.zip
* tensorboard --logdir output/log/LJSpeech

* synthesize.py 
* you can download [pretrained models](https://drive.google.com/drive/folders/1DOhZGlTLMbbAAFZmZGDdc77kz1PloS7F?usp=sharing) and put them in ``output/ckpt/LJSpeech/``
* The generated utterances will be put in ``output/result/``.

* evaluate.py

## gitlab code [here](https://gitlab.com/pyTUT/fastspeech2/-/commits/master)
